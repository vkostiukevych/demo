package com.example.demo.controller;

import com.example.demo.client.RestClient;
import com.example.demo.entity.Entity;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @author v.kostiukevych
 * date 19.04.18
 */
@RunWith(MockitoJUnitRunner.class)
public class ControllerTest {

    @Mock
    private RestClient restClient;

    @InjectMocks
    private Controller controller;

    @Before
    public void setUp() {
        when(restClient.getAll()).thenReturn(
                Arrays.asList(
                        Entity.builder().id(1L).userId(1L).title("title1").body("body1").build(),
                        Entity.builder().id(2L).userId(1L).title("title2").body("body2").build(),
                        Entity.builder().id(3L).userId(2L).title("title3").body("body3").build()
                )
        );

        when(restClient.getById(1L)).thenReturn(
                Optional.of(Entity.builder().id(1L).userId(1L).title("title1").body("body1").build())
        );

        when(restClient.getById(101L)).thenReturn(Optional.empty());
    }

    @Test
    public void getAll() {
        val expectedList = Arrays.asList(
                Entity.builder().id(1L).userId(1L).title("title1").body("body1").build(),
                Entity.builder().id(2L).userId(1L).title("title2").body("body2").build(),
                Entity.builder().id(3L).userId(2L).title("title3").body("body3").build()
        );
        val actualList = controller.getAll(null);

        assertEquals(expectedList, actualList);

        verify(restClient).getAll();
        verifyNoMoreInteractions(restClient);
    }

    @Test
    public void getAllWithFilter() {
        val expectedList = Arrays.asList(
                Entity.builder().id(1L).userId(1L).title("title1").body("body1").build(),
                Entity.builder().id(2L).userId(1L).title("title2").body("body2").build()
        );
        val actualList = controller.getAll(2L);

        assertEquals(expectedList, actualList);

        verify(restClient).getAll();
        verifyNoMoreInteractions(restClient);
    }

    @Test
    public void getById() {
        val expectedValue = ResponseEntity.ok(Entity.builder().id(1L).userId(1L).title("title1").body("body1").build());
        val actualValue = controller.getById(1L);

        assertEquals(expectedValue, actualValue);

        verify(restClient).getById(1L);
        verifyNoMoreInteractions(restClient);
    }

    @Test
    public void getByIdNotFound() {
        val expectedValue = ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        val actualValue = controller.getById(101L);

        assertEquals(expectedValue, actualValue);

        verify(restClient).getById(101L);
        verifyNoMoreInteractions(restClient);
    }

}
