package com.example.demo.controller;

import com.example.demo.client.RestClient;
import com.example.demo.entity.Entity;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * @author v.kostiukevych
 * date 19.04.18
 */
@RestController
@RequestMapping(path = "entities")
public class Controller {

    private final RestClient restClient;

    @Autowired
    public Controller(final RestClient restClient) {
        this.restClient = restClient;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Entity> getAll(@RequestParam(name = "filter", required = false) final Long filter) {
        val entities = restClient.getAll();

        return isNull(filter) ? entities :
                entities.stream()
                        .filter(entity -> filter.compareTo(entity.getId()) >= 0)
                        .collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Entity> getById(@PathVariable final Long id) {
        return restClient.getById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

}
