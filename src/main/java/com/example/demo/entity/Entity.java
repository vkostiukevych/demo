package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author v.kostiukevych
 * date 19.04.18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Entity {

    private Long userId;

    private Long id;

    private String title;

    private String body;

}
