package com.example.demo.client;

import com.example.demo.entity.Entity;

import java.util.List;
import java.util.Optional;

/**
 * @author v.kostiukevych
 * date 19.04.18
 */
public interface RestClient {

    List<Entity> getAll();

    Optional<Entity> getById(Long id);

}
