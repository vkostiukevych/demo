package com.example.demo.client;

import com.example.demo.entity.Entity;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

/**
 * @author v.kostiukevych
 * date 19.04.18
 */
@Service
public class RestClientImpl implements RestClient {

    private static final String URL = "https://jsonplaceholder.typicode.com/posts";

    private final RestTemplate restTemplate = new RestTemplate();

    @Override
    public List<Entity> getAll() {
        return restTemplate.exchange(
                URL,
                HttpMethod.GET,
                new HttpEntity(null),
                new ParameterizedTypeReference<List<Entity>>() {}
        ).getBody();
    }

    @Override
    public Optional<Entity> getById(final Long id) {
        try {
            return Optional.of(
                    restTemplate.exchange(
                            URL + "/{id}",
                            HttpMethod.GET,
                            new HttpEntity(null),
                            Entity.class,
                            id
                    ).getBody()
            );
        } catch (RestClientException e) {
            return Optional.empty();
        }
    }

}
